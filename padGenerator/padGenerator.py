import math
import numpy as np
from dxfwrite import DXFEngine as dxf


def ellipseCircumference(a, b):
    '''
    Calculates ellipse circumference based on
    Ramanujan's first (more precise) approximation
    '''
    return np.pi * (3 * (a + b) - np.sqrt(10*a*b + 3 * (a**2 + b**2)))


class Pad:
    def __init__(self,
                 EllipseRadia,  # [a, b]
                 PadHeight,  # [min, max]
                 PadThickness,  # [thickness_a, thickness_b]
                 PadIndent  # indent value equal for both sides
                 ):

        self.EllipseRadia = EllipseRadia
        self.PadHeight = [min(PadHeight), max(PadHeight)]
        self.PadThickness = PadThickness
        self.PadIndent = PadIndent

        fileformat = ".dxf"
        self.OutputFilenames = {
            "Outer": "PadOuterSide" + fileformat,
            "Inner": "PadInnerSide" + fileformat,
            "Circle": "PadCircle" + fileformat
        }

    def drawLine(self, a_Drawing, a_Point0, a_Point1):
        a_Drawing.add(dxf.line(a_Point0, a_Point1, color=7, layer='LINES'))

    def getDist(self, a_Point0, a_Point1):
        dx = np.fabs(a_Point0[0] - a_Point1[0])
        dy = np.fabs(a_Point0[1] - a_Point1[1])

        return np.sqrt(dx**2 + dy**2)

    @staticmethod
    def getPadHeightS(x, a, padHeight):
        return padHeight[0] + ((x + a) / (2 * a)) * (padHeight[1] - padHeight[0])


    def getPadHeight(self, x):
        return self.getPadHeightS(x, self.EllipseRadia[0], self.PadHeight)


    def genPadCircle(self, a_DataPoints=1000):
        '''
        Generates pad circle DXF.
        Data points are iterated with circle's angle
        '''

        drawing = dxf.drawing(self.OutputFilenames["Circle"])
        drawing.add_layer('LINES')

        ''' Generate outer circle '''
        # This stems from the tilt of pad's face
        a = np.sqrt((self.PadHeight[0] - self.PadHeight[1]) ** 2 + self.EllipseRadia[0] ** 2)
        b = self.EllipseRadia[1]

        tiltRatio = a / self.EllipseRadia[0]

        x_prev = a * np.sin(0.0)
        y_prev = b * np.cos(0.0)
        for i in range(1, a_DataPoints):
            angle = i * 2 * np.pi / a_DataPoints
            x = a * np.sin(angle)
            y = b * np.cos(angle)

            drawing.add(dxf.line((x_prev, y_prev),
                                 (x, y),
                                 color=7, layer='LINES'))
            x_prev = x
            y_prev = y

        ''' Generate inner circle '''
        # This stems from the tilt of pad's face
        a = (self.EllipseRadia[0] - self.PadThickness[0]) * tiltRatio
        b = self.EllipseRadia[1] - self.PadThickness[1]

        x_indent = (self.EllipseRadia[0] - self.PadThickness[0] - self.PadIndent) * tiltRatio
        y_indent = b * np.sqrt(1.0 - x_indent ** 2 / a ** 2) * tiltRatio

        angle_first = np.arctan2(x_indent, y_indent)

        angle_ranges = [angle_first,
                        np.pi - angle_first,
                        np.pi + angle_first,
                        2 * np.pi - angle_first]

        x_prev = a * np.sin(0.0)
        y_prev = b * np.cos(0.0)
        for i in range(1, a_DataPoints):
            angle = i * 2 * np.pi / a_DataPoints
            x = a * np.sin(angle)
            y = b * np.cos(angle)

            # right indent lines
            if angle_ranges[0] <= angle <= angle_ranges[1]:
                drawing.add(dxf.line((x_indent, y_prev),
                                     (x_indent, y),
                                     color=7, layer='LINES'))
            # left indent lines
            elif angle_ranges[2] <= angle <= angle_ranges[3]:
                drawing.add(dxf.line((-x_indent, y_prev),
                                     (-x_indent, y),
                                     color=7, layer='LINES'))
            # non-indent circle lines
            else:
                drawing.add(dxf.line((x_prev, y_prev),
                                     (x, y),
                                     color=7, layer='LINES'))
            x_prev = x
            y_prev = y

        drawing.save()

    def genPadOuterSide(self, a_DataPoints=1000):
        '''
        Generates pad outer side DXF.
        Pad height as a function of distance along 'a' axis, where -a < x < +a
            h(x) = H1 + ((x + a) / 2a) * (H2 - H1)
            where:
                h(x)    - height function
                H1      - minimal height
                H2      - maximal height
                2a      - 2x ellipse 'a' radius
        '''

        drawing = dxf.drawing(self.OutputFilenames["Outer"])
        drawing.add_layer('LINES')

        a = self.EllipseRadia[0]
        b = self.EllipseRadia[1]

        circumference = ellipseCircumference(a, b)

        ''' Iterate over circle angle '''
        x_prev = a * np.cos(0.0 - np.pi)
        h_prev = self.getPadHeight(x_prev)
        d_prev = -circumference / 2.0

        drawing.add(dxf.line((d_prev, 0),
                             (d_prev, h_prev),
                             color=7, layer='LINES'))
        drawing.add(dxf.line((-d_prev, 0),
                             (-d_prev, h_prev),
                             color=7, layer='LINES'))

        for i in range(1, a_DataPoints):
            angle = i * 2 * np.pi / a_DataPoints
            d = i * circumference / a_DataPoints - circumference / 2.0

            x = a * np.cos(angle - np.pi)
            h = self.getPadHeight(x)

            drawing.add(dxf.line((d_prev, 0),
                                 (d, 0),
                                 color=7, layer='LINES'))

            drawing.add(dxf.line((d_prev, h_prev),
                                 (d, h),
                                 color=7, layer='LINES'))

            d_prev = d
            h_prev = h

        drawing.save()

    def genPadInnerSide(self, a_DataPoints=1000):
        '''
        Generates pad inner side DXF.
        '''

        drawing = dxf.drawing(self.OutputFilenames["Inner"])
        drawing.add_layer('LINES')

        a = self.EllipseRadia[0] - self.PadThickness[0]
        b = self.EllipseRadia[1] - self.PadThickness[1]

        x_indent = self.EllipseRadia[0] - self.PadThickness[0] - self.PadIndent
        y_indent = b * np.sqrt(1.0 - x_indent ** 2 / a ** 2)

        angle_first = np.pi/2 - np.arctan2(x_indent, y_indent)

        angle_ranges = [0.0,
                        angle_first,
                        np.pi - angle_first,
                        np.pi]

        x_prev = x_indent
        y_prev = x_prev * np.tan(0.0)
        d_prev = 0
        h_prev = self.getPadHeight(x_prev)
        for i in range(1, round(a_DataPoints/2)):
            angle = i * 2 * np.pi / a_DataPoints
            # 0 < angle < pi

            if angle_ranges[0] < angle < angle_ranges[1]:
                x = x_indent
                y = x * np.tan(angle)
            elif angle_ranges[2] < angle < angle_ranges[3]:
                x = -x_indent
                y = x * np.tan(angle)
            else:
                x = a * np.cos(angle)
                y = b * np.sin(angle)

            d = self.getDist([x_prev, y_prev], [x, y])
            h = self.getPadHeight(x)

            drawing.add(dxf.line((d_prev, 0),
                                 (d_prev + d, 0),
                                 color=7, layer='LINES'))
            drawing.add(dxf.line((-d_prev, 0),
                                 (-(d_prev + d), 0),
                                 color=7, layer='LINES'))
            drawing.add(dxf.line((d_prev, h_prev),
                                 (d_prev + d, h),
                                 color=7, layer='LINES'))
            drawing.add(dxf.line((-d_prev, h_prev),
                                 (-(d_prev + d), h),
                                 color=7, layer='LINES'))

            x_prev = x
            y_prev = y
            d_prev += d
            h_prev = h

        drawing.add(dxf.line((d_prev, 0),
                             (d_prev, h),
                             color=7, layer='LINES'))
        drawing.add(dxf.line((-d_prev, 0),
                             (-d_prev, h),
                             color=7, layer='LINES'))
        drawing.add(dxf.line((0, 0),
                             (0, self.getPadHeight(x_indent)),
                             color=7, layer='LINES'))

        drawing.save()


    def genAllDrawings(self):
        self.genPadCircle()
        self.genPadOuterSide()
        self.genPadInnerSide()
