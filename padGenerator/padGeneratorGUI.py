import tkinter as tk
import numpy as np
import padGenerator as pg


class ColorPalettes:
    def __init__(self):
        print("Init")

    GrayBlue = {
        "1": "#E4E3E3",
        "2": "#84A9AC",
        "3": "#3B6978",
        "4": "#204051"
    }

    LightBlue = {
        "1": "#D9D9D9",
        "2": "#D6E6F2",
        "3": "#B9D7EA",
        "4": "#769FCD"
    }


class PadTopDrawing:
    def __init__(self, a_Parent=None, a_Size=[600, 600], a_MaxSizeMM=[150, 150]):
        self.m_Parent = a_Parent
        self.m_Canvas = tk.Canvas(self.m_Parent, width=a_Size[0], height=a_Size[1])
        self.m_Canvas.pack()

        self.m_Center = [0.5 * a_Size[0], 0.5 * a_Size[1]]
        self.m_Size = a_Size
        self.m_DrawRatio = [a_Size[0] / a_MaxSizeMM[0], a_Size[1] / a_MaxSizeMM[1]]

        self.m_OvalOuter = self.m_Canvas.create_oval(0, 0, a_Size[0], a_Size[1], width=2,
                                                     fill=ColorPalettes.LightBlue["3"])
        self.m_OvalInner = self.m_Canvas.create_oval(100, 100, a_Size[0] - 100, a_Size[0] - 100, width=2,
                                                     fill=ColorPalettes.LightBlue["1"])

        self.m_Rect1 = self.m_Canvas.create_rectangle([10, 10, 150, 150], width=0, fill=ColorPalettes.LightBlue["3"])
        self.m_Rect2 = self.m_Canvas.create_rectangle([10, 10, 1050, 150], width=0, fill=ColorPalettes.LightBlue["3"])

        self.m_Line1 = self.m_Canvas.create_line(10, 10, 150, 150, width=2)
        self.m_Line2 = self.m_Canvas.create_line(10, 10, 150, 150, width=2)

        self.m_MeasureLines = {
            "InnerWidth": self.m_Canvas.create_line(10, 10, 150, 150, width=2, fill='#FF0000'),
            "InnerWidthLabel": self.m_Canvas.create_text(self.m_Center[0] + 50, self.m_Center[1], fill='#FF0000'),
            "InnerHeight": self.m_Canvas.create_line(10, 10, 150, 150, width=2, fill='#FF0000'),
            "InnerHeightLabel": self.m_Canvas.create_text(self.m_Center[0], self.m_Center[1] - 50, fill='#FF0000')
        }

    def redraw(self, a_CoordsOuter, a_CoordsInner, a_PadIndent):
        outerCoords = [self.m_Center[0] - self.m_DrawRatio[0] * a_CoordsOuter[0] / 2,
                       self.m_Center[0] - self.m_DrawRatio[0] * a_CoordsOuter[1] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * a_CoordsOuter[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * a_CoordsOuter[1] / 2]

        innerCoords = [self.m_Center[0] - self.m_DrawRatio[0] * a_CoordsInner[0] / 2,
                       self.m_Center[0] - self.m_DrawRatio[0] * a_CoordsInner[1] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * a_CoordsInner[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * a_CoordsInner[1] / 2]

        a = a_CoordsInner[0] / 2 * self.m_DrawRatio[0]
        b = a_CoordsInner[1] / 2 * self.m_DrawRatio[1]
        x = (a - a_PadIndent * self.m_DrawRatio[0])
        y = (b * np.sqrt(1.0 - x ** 2 / a ** 2))

        line1Coords = [self.m_Center[0] - x,
                       self.m_Center[1] - y,
                       self.m_Center[0] - x,
                       self.m_Center[1] + y]

        line2Coords = [self.m_Center[0] + x,
                       self.m_Center[1] - y,
                       self.m_Center[0] + x,
                       self.m_Center[1] + y]

        self.m_Canvas.coords(self.m_OvalOuter, outerCoords)
        self.m_Canvas.coords(self.m_OvalInner, innerCoords)

        self.m_Canvas.coords(self.m_Rect1, [line1Coords[0],
                                            line1Coords[1],
                                            innerCoords[0],
                                            line1Coords[3]])

        self.m_Canvas.coords(self.m_Rect2, [line2Coords[0],
                                            line2Coords[1],
                                            innerCoords[2],
                                            line2Coords[3]])

        self.m_Canvas.coords(self.m_Line1, line1Coords)
        self.m_Canvas.coords(self.m_Line2, line2Coords)

        ''' Draw measuring lines '''
        self.m_Canvas.coords(self.m_MeasureLines["InnerWidth"], [line1Coords[0],
                                                                 innerCoords[3] + 10,
                                                                 line2Coords[0],
                                                                 innerCoords[3] + 10])
        self.m_Canvas.coords(self.m_MeasureLines["InnerHeight"], [innerCoords[2] + 10,
                                                                  innerCoords[1],
                                                                  innerCoords[2] + 10,
                                                                  innerCoords[3]])

        self.m_Canvas.coords(self.m_MeasureLines["InnerWidthLabel"], [self.m_Center[0],
                                                                      innerCoords[3] + 20])

        self.m_Canvas.coords(self.m_MeasureLines["InnerHeightLabel"], [innerCoords[2] + 20,
                                                                       self.m_Center[1]])

        self.m_Canvas.itemconfig(self.m_MeasureLines["InnerWidthLabel"],
                                 text="{}mm".format(a_CoordsInner[0] - 2 * a_PadIndent))

        self.m_Canvas.itemconfig(self.m_MeasureLines["InnerHeightLabel"],
                                 text="{}mm".format(a_CoordsInner[1]),
                                 angle=90)


class PadSideDrawing:
    def __init__(self, a_Parent=None, a_Size=[600, 298], a_MaxSizeMM=[150, 75]):
        self.m_Parent = a_Parent
        self.m_Canvas = tk.Canvas(self.m_Parent, width=a_Size[0], height=a_Size[1])
        self.m_Canvas.pack()

        self.m_Center = [0.5 * a_Size[0], 0.5 * a_Size[1]]
        self.m_Size = a_Size
        self.m_DrawRatio = [a_Size[0] / a_MaxSizeMM[0], a_Size[1] / a_MaxSizeMM[1]]

        self.m_PolyA = self.m_Canvas.create_polygon([10, 10, 20, 10, 30, 20, 40, 50], outline='#000000', width=2,
                                                    fill=ColorPalettes.LightBlue["3"])
        self.m_PolyB = self.m_Canvas.create_polygon([10, 10, 20, 10, 30, 20, 40, 50],
                                                    fill=ColorPalettes.LightBlue["2"])

        self.m_AngleIndicator = {
            "LineHorizontal": self.m_Canvas.create_line([10, 10, 10, 10], width=2, fill='#FF0000'),
            "LineAngled": self.m_Canvas.create_line([10, 10, 10, 10], width=2, fill='#FF0000'),
            "AngleArc": self.m_Canvas.create_arc(10, 10, 10, 10, width=2, outline='#FF0000'),
            "TextLabel": self.m_Canvas.create_text(self.m_Center[0], self.m_Center[1], fill='#FF0000')
        }

    def redraw(self, a_Thickness, a_OuterDia, a_InnerDia):
        T0 = min(a_Thickness)
        T1 = max(a_Thickness)

        polyCoordsA = [self.m_Center[0] - self.m_DrawRatio[0] * a_OuterDia[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * T1 / 2,

                       self.m_Center[0] + self.m_DrawRatio[0] * a_OuterDia[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * T1 / 2,

                       self.m_Center[0] + self.m_DrawRatio[0] * a_OuterDia[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * (T1 / 2 - a_Thickness[1]),

                       self.m_Center[0] - self.m_DrawRatio[0] * a_OuterDia[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * (T1 / 2 - a_Thickness[0])]

        thickDiff = abs(a_Thickness[1] - a_Thickness[0])
        thickSlope = np.sign(a_Thickness[1] - a_Thickness[0])
        innerThickness = [
            min(a_Thickness) + (a_OuterDia[0] / 2 - thickSlope * a_InnerDia[0] / 2) / a_OuterDia[0] * thickDiff,
            min(a_Thickness) + (a_OuterDia[0] / 2 + thickSlope * a_InnerDia[0] / 2) / a_OuterDia[0] * thickDiff]

        polyCoordsB = [self.m_Center[0] - self.m_DrawRatio[0] * a_InnerDia[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * T1 / 2 - 1,

                       self.m_Center[0] + self.m_DrawRatio[0] * a_InnerDia[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * T1 / 2 - 1,

                       self.m_Center[0] + self.m_DrawRatio[0] * a_InnerDia[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * (T1 / 2 - innerThickness[1]) + 1,

                       self.m_Center[0] - self.m_DrawRatio[0] * a_InnerDia[0] / 2,
                       self.m_Center[1] + self.m_DrawRatio[1] * (T1 / 2 - innerThickness[0]) + 1]

        self.m_Canvas.coords(self.m_PolyA, polyCoordsA)
        self.m_Canvas.coords(self.m_PolyB, polyCoordsB)

        ''' Angle indicator '''
        self.m_Canvas.coords(self.m_AngleIndicator["LineHorizontal"],
                             [polyCoordsA[0],
                              polyCoordsA[7],
                              self.m_Center[0] - self.m_DrawRatio[0] * a_OuterDia[0] / 4,
                              polyCoordsA[7]
                              ])
        self.m_Canvas.coords(self.m_AngleIndicator["LineAngled"],
                             [polyCoordsA[0],
                              polyCoordsA[7],
                              self.m_Center[0] - self.m_DrawRatio[0] * a_OuterDia[0] / 4,
                              polyCoordsA[1] - self.m_DrawRatio[0] * pg.Pad.getPadHeightS(-a_OuterDia[0] / 4,
                                                                                          a_OuterDia[0] / 2,
                                                                                          a_Thickness)
                              ])
        self.m_Canvas.coords(self.m_AngleIndicator["AngleArc"],
                             [polyCoordsA[0] - 50,
                              polyCoordsA[7] - 50,
                              polyCoordsA[0] + 50,
                              polyCoordsA[7] + 50
                              ])

        angle = 180 * np.arctan2(a_Thickness[1] - a_Thickness[0], a_OuterDia[0]) / np.pi
        self.m_Canvas.itemconfig(self.m_AngleIndicator["AngleArc"], start=0.0, extent=angle)
        self.m_Canvas.coords(self.m_AngleIndicator["TextLabel"], [self.m_Center[0], polyCoordsA[7]])
        self.m_Canvas.itemconfig(self.m_AngleIndicator["TextLabel"], text="{:.2f}\u00b0".format(angle))


class PadFrontDrawing:
    def __init__(self, a_Parent=None, a_Size=[600, 298], a_MaxSizeMM=[150, 75]):
        self.m_Parent = a_Parent
        self.m_Canvas = tk.Canvas(self.m_Parent, width=a_Size[0], height=a_Size[1])
        self.m_Canvas.pack()

        self.m_Center = [0.5 * a_Size[0], 0.5 * a_Size[1]]
        self.m_Size = a_Size
        self.m_DrawRatio = [a_Size[0] / a_MaxSizeMM[0], a_Size[1] / a_MaxSizeMM[1]]

        self.m_RectOuter = self.m_Canvas.create_rectangle([10, 10, 100, 100], outline='#000000', width=2,
                                                          fill=ColorPalettes.LightBlue["3"])
        self.m_OvalOuter = self.m_Canvas.create_oval(0, 0, a_Size[0], a_Size[1], width=2,
                                                     fill=ColorPalettes.LightBlue["3"])
        self.m_OvalInner = self.m_Canvas.create_oval(0, 0, a_Size[0], a_Size[1], width=2,
                                                     fill=ColorPalettes.LightBlue["2"])

    def redraw(self, a_Thickness, a_OuterDia, a_InnerDia):
        averageThick = np.mean(a_Thickness)
        thick = max(a_Thickness)

        thickDiff = abs(a_Thickness[1] - a_Thickness[0])
        innerThicknessDiff = (a_OuterDia[0] / 2 - a_InnerDia[0] / 2) / a_OuterDia[0] * thickDiff

        rectOuterCoords = [self.m_Center[0] - self.m_DrawRatio[0] * a_OuterDia[1] / 2,
                           self.m_Center[1] - self.m_DrawRatio[1] * (averageThick - thick / 2),
                           self.m_Center[0] + self.m_DrawRatio[0] * a_OuterDia[1] / 2,
                           self.m_Center[1] + self.m_DrawRatio[1] * thick / 2]

        ovalOuterCoords = [self.m_Center[0] - self.m_DrawRatio[0] * a_OuterDia[1] / 2,
                           self.m_Center[1] - self.m_DrawRatio[1] * thick / 2,
                           self.m_Center[0] + self.m_DrawRatio[0] * a_OuterDia[1] / 2,
                           self.m_Center[1] + self.m_DrawRatio[1] * (thick / 2 - min(a_Thickness))]

        ovalInnerCoords = [self.m_Center[0] - self.m_DrawRatio[0] * a_InnerDia[1] / 2,
                           self.m_Center[1] - self.m_DrawRatio[1] * (thick / 2 - innerThicknessDiff),
                           self.m_Center[0] + self.m_DrawRatio[0] * a_InnerDia[1] / 2,
                           self.m_Center[1] + self.m_DrawRatio[1] * (thick / 2 - min(a_Thickness) - innerThicknessDiff)]

        self.m_Canvas.coords(self.m_RectOuter, rectOuterCoords)
        self.m_Canvas.coords(self.m_OvalOuter, ovalOuterCoords)
        self.m_Canvas.coords(self.m_OvalInner, ovalInnerCoords)


class PadGuiApp(tk.Frame):
    def __init__(self, a_Parent=None):
        super().__init__(a_Parent)
        self.m_Parent = a_Parent
        self.setWindowSize()
        self.initFrames()
        self.create_widgets()
        a_Parent.title("BosonAudio Pad Generator")


    def initFrames(self):
        self.m_Sliders = tk.Frame(self.m_Parent, highlightbackground="black", highlightthickness=0)
        self.m_Sliders.grid(row=0, column=0)

        self.m_FramePadTop = tk.Frame(self.m_Parent, highlightbackground="black", highlightthickness=0)
        self.m_FramePadTop.grid(row=0, column=1)

        self.m_FramePadSide = tk.Frame(self.m_Parent, highlightbackground="black", highlightthickness=0)
        self.m_FramePadSide.grid(row=0, column=2)

        self.m_FramePadSideA = tk.Frame(self.m_FramePadSide, highlightbackground="black", highlightthickness=0)
        self.m_FramePadSideA.grid(row=0, column=0)

        self.m_FramePadSideB = tk.Frame(self.m_FramePadSide, highlightbackground="black", highlightthickness=0)
        self.m_FramePadSideB.grid(row=1, column=0)


    def setWindowSize(self, a_Percentage=70):
        screen_width = 0.005 * a_Percentage * self.m_Parent.winfo_screenwidth()
        screen_height = 0.01 * a_Percentage * self.m_Parent.winfo_screenheight()

        self.m_ScreenSize = [screen_width, screen_height]

        self.m_Parent.geometry("{}x{}".format(int(screen_width), int(screen_height)))

    def onGenerateButtonClick(self):
        padGenetator = pg.Pad(
            [self.m_SliderOuterDiameterA.get() / 2.0, self.m_SliderOuterDiameterB.get() / 2.0],
            [self.m_SliderPadHeightA.get(), self.m_SliderPadHeightB.get()],
            [self.m_SliderThicknessA.get(), self.m_SliderThicknessB.get()],
            self.m_SliderPadIndent.get())

        padGenetator.genAllDrawings()

    def create_widgets(self):
        self.m_SliderOuterDiameterA = tk.Scale(
            self.m_Sliders,
            from_=150,
            to=30,
            tickinterval=1,
            length=200,
            command=self.onSlidersChange)
        self.m_SliderOuterDiameterA.grid(row=0, column=0)
        self.m_SliderOuterDiameterA.set(100)

        self.m_SliderOuterDiameterB = tk.Scale(
            self.m_Sliders,
            from_=150,
            to=30,
            length=200,
            command=self.onSlidersChange)
        self.m_SliderOuterDiameterB.grid(row=0, column=1)
        self.m_SliderOuterDiameterB.set(100)

        self.m_SliderThicknessA = tk.Scale(
            self.m_Sliders,
            from_=60,
            to=10,
            tickinterval=1,
            length=100,
            command=self.onSlidersChange)
        self.m_SliderThicknessA.grid(row=1, column=0)
        self.m_SliderThicknessA.set(20)

        self.m_SliderThicknessB = tk.Scale(
            self.m_Sliders,
            from_=60,
            to=10,
            length=100,
            command=self.onSlidersChange)
        self.m_SliderThicknessB.grid(row=1, column=1)
        self.m_SliderThicknessB.set(20)

        self.m_SliderPadHeightA = tk.Scale(
            self.m_Sliders,
            from_=60,
            to=5,
            tickinterval=1,
            length=150,
            command=self.onSlidersChange)
        self.m_SliderPadHeightA.grid(row=2, column=0)
        self.m_SliderPadHeightA.set(15)

        self.m_SliderPadHeightB = tk.Scale(
            self.m_Sliders,
            from_=60,
            to=5,
            length=150,
            command=self.onSlidersChange)
        self.m_SliderPadHeightB.grid(row=2, column=1)
        self.m_SliderPadHeightB.set(30)

        self.m_SliderPadIndent = tk.Scale(
            self.m_Sliders,
            from_=self.m_SliderOuterDiameterA.get() - self.m_SliderThicknessA.get(),
            to=0,
            tickinterval=1,
            length=100,
            command=self.onSlidersChange)
        self.m_SliderPadIndent.grid(row=3, column=0)
        self.m_SliderPadIndent.set(0)

        self.m_GenerateButton = tk.Button(
            self.m_Sliders,
            text="Generate",
            command=self.onGenerateButtonClick,
            width=12,
            height=2)
        self.m_GenerateButton.grid(row=4, column=0, columnspan=2)

        self.m_CanvasPadTop = PadTopDrawing(self.m_FramePadTop)
        self.m_CanvasPadSideA = PadSideDrawing(self.m_FramePadSideA)
        self.m_CanvasPadSideB = PadFrontDrawing(self.m_FramePadSideB)
        self.recalcDraws()

    def onSlidersChange(self, a_Value):
        self.recalcSliders()
        self.recalcDraws()

    def recalcSliders(self):
        sThickA = self.m_SliderThicknessA.get()
        sOutDiaA = self.m_SliderOuterDiameterA.get()
        sOutDiaB = self.m_SliderOuterDiameterB.get()

        ''' Limit thickness and indent to prevent overlapping '''
        minClearence = 5.0
        self.m_SliderThicknessA.configure(from_=(sOutDiaA / 2.0 - minClearence))
        self.m_SliderThicknessB.configure(from_=(sOutDiaB / 2.0 - minClearence))
        self.m_SliderPadIndent.configure(from_=(sOutDiaA / 2.0 - sThickA - minClearence))

    def recalcDraws(self):
        sOutDiaA = self.m_SliderOuterDiameterA.get()
        sOutDiaB = self.m_SliderOuterDiameterB.get()
        sInnDiaA = sOutDiaA - 2 * self.m_SliderThicknessA.get()
        sInnDiaB = sOutDiaB - 2 * self.m_SliderThicknessB.get()

        sHeightA = self.m_SliderPadHeightA.get()
        sHeightB = self.m_SliderPadHeightB.get()

        self.m_CanvasPadTop.redraw(
            [sOutDiaA, sOutDiaB],
            [sInnDiaA, sInnDiaB],
            self.m_SliderPadIndent.get())

        self.m_CanvasPadSideA.redraw(
            [sHeightA, sHeightB],
            [sOutDiaA, sOutDiaB],
            [sInnDiaA, sInnDiaB])

        self.m_CanvasPadSideB.redraw(
            [sHeightA, sHeightB],
            [sOutDiaA, sOutDiaB],
            [sInnDiaA, sInnDiaB])


def main():
    root = tk.Tk()
    app = PadGuiApp(a_Parent=root)
    app.mainloop()


if __name__ == "__main__":
    main()
